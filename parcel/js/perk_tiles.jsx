import React from 'react';

import {PerkTile} from './perk_tile';

export function PerkTiles(props) {
  const {perks, noPerk, onPerkSelection, selectedPrice, selectedPerk, setPerkOption, perkOption, frequency} = props;

  return (
    <div className="perks">
      {perks.map((perk) =>
        <PerkTile perk={perk} noPerk={noPerk} onPerkSelection={onPerkSelection} selectedPrice={selectedPrice} selectedPerk={selectedPerk} key={perk.name} setPerkOption={setPerkOption} perkOption={perkOption} frequency={frequency}/>
      )}
    </div>
  );
}
