import React from 'react';
import {PriceButtons} from './price_buttons';
import {PriceOther} from './price_other';

export function DonationPrices(props) {
  const {onPriceChange, selectedPrice, pricesOnButtons, priceOtherUseRef} = props;

  return (
    <div className="donate-buttons">
      <PriceButtons
        onPriceChange={onPriceChange}
        pricesOnButtons={pricesOnButtons}
        selectedPrice={selectedPrice}
       />
      <PriceOther
        onPriceChange={onPriceChange}
        selectedPrice={selectedPrice}
        priceOtherUseRef={priceOtherUseRef}
      />
    </div>
  );
}
