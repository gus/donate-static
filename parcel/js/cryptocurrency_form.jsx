import React, {useState, useEffect} from 'react';
import {Checkbox} from './checkbox';
import {WalletAddresses} from './wallet_addresses';
import {AmountField} from './amount_field';
import {t} from './i18n';

export function CryptocurrencyForm(props) {
  const [donateAnonymously, setDonateAnonymously] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState('US');
  const [fieldErrors, setFieldErrors] = useState([]);
  const {wallets, donateProccessorBaseUrl} = props;

  const countryChanged = (event) => {
    setSelectedCountry(event.target.value);
  };

  const onAnonymousCheckboxChange = (event) => {
    setDonateAnonymously(event.target.checked);
  };

  const onFieldValidate = (fieldName, valid) => {
    if (valid) {
      const pos = fieldErrors.indexOf(fieldName);
      fieldErrors.splice(pos, 1);
      setFieldErrors([...fieldErrors]);
    } else {
      setFieldErrors([fieldName, ...fieldErrors]);
    }
  };

  const onSubmit = (event) => {
    if (fieldErrors.length > 0) {
      event.preventDefault();
    }
  };

  const walletOptions = wallets.map((wallet) => {
    return (<option key={wallet.symbol} value={wallet.symbol}>{wallet.name} ({wallet.symbol})</option>)
  });

  let conditionalFields = null;
  if (!donateAnonymously) {
    conditionalFields = (
      <React.Fragment>
        <input className="field" name="firstName" placeholder={t('t-first-name')} maxLength="256" type="text" required />
        <input className="field" name="lastName" placeholder={t('t-last-name')} maxLength="256" type="text" />
      </React.Fragment>
    );
  }

  const requestDestination = donateProccessorBaseUrl + "/cryptocurrency/donate";

  return(
    <form action={requestDestination} method="POST" onSubmit={onSubmit}>
      <div className="section">
        <div className="form-column">
          <h4>{t('t-your-info')}</h4>
          <div className="form-fields">
            <div className="anonymous-selection checkbox-row">
              <Checkbox name="donateAnonymously" onChange={onAnonymousCheckboxChange} />
              <label className="light" htmlFor="donateAnonymously">{t('t-donate-anonymously')}</label>
            </div>
            {conditionalFields}
            <div className="mailing-list-opt-in checkbox-row">
              <input name="mailingListOptIn" id="mailingListOptIn" type="checkbox" />
              <label className="light" htmlFor="mailingListOptIn">{t('t-start-email-updates')}</label>
            </div>
            <input className="field" name="email" placeholder={t('t-email')} maxLength="256" type="text" required />
            <label htmlFor="estimatedDonationDate">{t('t-estimated-donation-date')}</label>
            <input name="estimatedDonationDate" id="estimatedDonationDate" placeholder={t('t-estimated-donation-date')} type="date" required />
            <select className="field required" name="cryptocurrencyType" required>
              <option value="">{t('t-choose-currency')}</option>
              {walletOptions}
            </select>
            <AmountField name="currencyAmount" placeHolder={t('t-currency-amount')} onFieldValidate={onFieldValidate} />
          </div>
        </div>
        <div className="wallet-column">
          <h4>{t('t-wallet-addresses')}</h4>
          <WalletAddresses wallets={wallets} />
        </div>
      </div>
      <div className="section button-section">
        <input className="donate button" type="submit" value={t('t-report-donation')}/>
      </div>
    </form>
  );
}
