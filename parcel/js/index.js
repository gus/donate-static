import 'babel-polyfill';
import {GivingForm} from './giving_form';
import {CampaignTotals} from './campaign_totals';
import {CryptocurrencyForm} from './cryptocurrency_form';
import React from 'react';
import ReactDOM from 'react-dom';
import {AppContext, initializeAppContext} from './app_context';
import {StripeProvider, Elements} from 'react-stripe-elements';

const reactCallbacks = {};
const availableComponents = {
  'GivingForm': GivingForm,
  'CampaignTotals': CampaignTotals,
  'CryptocurrencyForm': CryptocurrencyForm,
};

if ('reactComponents' in window) {
  for (const reactComponent of window.reactComponents) {
    const element = document.getElementById(reactComponent.id);
    const props = reactComponent.props;
    const appContext = initializeAppContext(props);
    if (element !== null) {
      const ComponentToUse = availableComponents[reactComponent.name];
      let markup = null;
      if ('stripePublishableKey' in props) {
        markup = (
          <AppContext.Provider value={appContext}>
            <StripeProvider apiKey={props.stripePublishableKey}>
              <Elements>
                <ComponentToUse callbacks={reactCallbacks} {...reactComponent.props} />
              </Elements>
            </StripeProvider>
          </AppContext.Provider>
        );
      } else {
        markup = (
          <AppContext.Provider value={appContext}>
              <ComponentToUse callbacks={reactCallbacks} {...reactComponent.props} />
          </AppContext.Provider>
        );
      }
      ReactDOM.render(markup, element);
    }
  }
}

window.tor = {
  reactCallbacks
};
