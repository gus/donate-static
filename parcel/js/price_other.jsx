import React, {useState} from 'react';

export function PriceOther(props) {
  const {onPriceChange, selectedPrice, priceOtherUseRef} = props;

  const [error, setError] = useState(null);

  const onClick = (event) => {
    return (event.target.placeholder= "");
  };

  const onBlur = (event) => {
    const input = event.target.value;
    if (input == '') {
      setError(null);
      return (event.target.placeholder = '$ other');
    } else {
      let newValue = parseInt(input);
      if (isNaN(newValue)) {
        setError(<div className="error">invalid amount</div>);
      } else {
        newValue = newValue * 100;
        onPriceChange(newValue);
        if (newValue >= 200) {
          setError(null);
        } else {
          setError(<div className="error">$2 minimum donation</div>);
        }
      }
    }
  }

  const classes = ['perk-amt'];
  if (error) {
    classes.push('error');
  }

  return (
    <div className="other-amount-field">
      {error}
      <input ref={priceOtherUseRef} className={classes.join(' ')} data-name="Other Amount" id="otherAmount" maxLength="256" name="otherAmount" placeholder="$ other" type="text" onClick={onClick} onBlur={onBlur} />
    </div>
  );
}
