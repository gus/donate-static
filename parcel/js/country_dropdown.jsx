import React from 'react';
import {useEffect} from 'react';
import {useState} from 'react';

export function CountryDropdown(props) {
  const {countries, countryChanged, selectedCountry, required} = props;

  const onChange = (args) => {
    countryChanged(args);
  };

  let optionElements = [];
  for (const country of countries) {
    const code = country[0];
    const name = country[1];
    optionElements.push(<option key={code} value={code}>{name}</option>);
  }

  let classes=['field']
  if (required) {
    classes.push('required');
  }

  return(
    <select id="country" name="country" className={classes.join(' ')} onChange={onChange} value={selectedCountry}>
      {optionElements}
    </select>
  );
}
